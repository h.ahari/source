import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './prisma/prisma.module';
import { CurrenciesModule } from './currencies/currencies.module';
import { ExchangesModule } from './exchanges/exchanges.module';
import { MarketsModule } from './markets/markets.module';
import { QuotesModule } from './quotes/quotes.module';
import { TwelvedataModule } from './twelvedata/twelvedata.module';
import { InfluxdbModule } from './influxdb/influxdb.module';

@Module({
  imports: [PrismaModule, CurrenciesModule, ExchangesModule, MarketsModule, QuotesModule, TwelvedataModule, InfluxdbModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
