import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CurrenciesService } from './currencies.service';
import { CreateCurrencyDto } from './dto/create-currency.dto';
import { UpdateCurrencyDto } from './dto/update-currency.dto';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CurrencyEntity } from './entities/currency.entity';

@Controller('currencies')
@ApiTags('Currency')
export class CurrenciesController {
  constructor(private readonly currenciesService: CurrenciesService) { }

  @Post()
  @ApiCreatedResponse({ type: CurrencyEntity })
  create(@Body() createCurrencyDto: CreateCurrencyDto) {
    return this.currenciesService.create(createCurrencyDto);
  }

  @Get()
  @ApiOkResponse({ type: CurrencyEntity, isArray: true })
  findAll() {
    return this.currenciesService.findAll();
  }

  @Get(':id')
  @ApiOkResponse({ type: CurrencyEntity })
  findOne(@Param('id') id: string) {
    return this.currenciesService.findOne(+id);
  }

  @Patch(':id')
  @ApiOkResponse({ type: CurrencyEntity })
  update(@Param('id') id: string, @Body() updateCurrencyDto: UpdateCurrencyDto) {
    return this.currenciesService.update(+id, updateCurrencyDto);
  }

  @Delete(':id')
  @ApiOkResponse({ type: CurrencyEntity })
  remove(@Param('id') id: string) {
    return this.currenciesService.remove(+id);
  }
}
