import { Injectable } from '@nestjs/common';
import { CreateCurrencyDto } from './dto/create-currency.dto';
import { UpdateCurrencyDto } from './dto/update-currency.dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class CurrenciesService {
  constructor(private prisma: PrismaService) { }

  create(createCurrencyDto: CreateCurrencyDto) {
    return this.prisma.currency.create({ data: createCurrencyDto });
  }

  findAll() {
    return this.prisma.currency.findMany();
  }

  findOne(id: number) {
    return this.prisma.currency.findUnique({ where: { id } });
  }

  update(id: number, updateCurrencyDto: UpdateCurrencyDto) {
    return `This action updates a #${id} currency`;
  }

  remove(id: number) {
    return `This action removes a #${id} currency`;
  }
}
