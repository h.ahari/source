import { ApiProperty } from "@nestjs/swagger"

export class CreateCurrencyDto {
  @ApiProperty()
  name: string

  @ApiProperty()
  symbol: string
}
