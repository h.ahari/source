import { ApiProperty } from "@nestjs/swagger";

export class CreateExchangeDto {
  @ApiProperty()
  name: string
}
