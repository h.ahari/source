import { ApiProperty } from "@nestjs/swagger";
import { Exchange } from "@prisma/client";

export class ExchangeEntity implements Exchange {
  @ApiProperty()
  id: number

  @ApiProperty()
  name: string
}
