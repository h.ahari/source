import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ExchangesService } from './exchanges.service';
import { CreateExchangeDto } from './dto/create-exchange.dto';
import { UpdateExchangeDto } from './dto/update-exchange.dto';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ExchangeEntity } from './entities/exchange.entity';

@Controller('exchanges')
@ApiTags('Exchange')
export class ExchangesController {
  constructor(private readonly exchangesService: ExchangesService) { }

  @Post()
  @ApiCreatedResponse({ type: ExchangeEntity })
  create(@Body() createExchangeDto: CreateExchangeDto) {
    return this.exchangesService.create(createExchangeDto);
  }

  @Get()
  @ApiOkResponse({ type: ExchangeEntity, isArray: true })
  findAll() {
    return this.exchangesService.findAll();
  }

  @Get(':id')
  @ApiOkResponse({ type: ExchangeEntity })
  findOne(@Param('id') id: string) {
    return this.exchangesService.findOne(+id);
  }

  @Patch(':id')
  @ApiOkResponse({ type: ExchangeEntity })
  update(@Param('id') id: string, @Body() updateExchangeDto: UpdateExchangeDto) {
    return this.exchangesService.update(+id, updateExchangeDto);
  }

  @Delete(':id')
  @ApiOkResponse({ type: ExchangeEntity })
  remove(@Param('id') id: string) {
    return this.exchangesService.remove(+id);
  }
}
