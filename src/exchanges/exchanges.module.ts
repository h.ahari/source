import { Module } from '@nestjs/common';
import { ExchangesService } from './exchanges.service';
import { ExchangesController } from './exchanges.controller';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
  controllers: [ExchangesController],
  providers: [ExchangesService],
  imports: [PrismaModule],
})
export class ExchangesModule { }
