import { Injectable } from '@nestjs/common';
import { CreateExchangeDto } from './dto/create-exchange.dto';
import { UpdateExchangeDto } from './dto/update-exchange.dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class ExchangesService {
  constructor(private prisma: PrismaService) { }

  create(createExchangeDto: CreateExchangeDto) {
    return this.prisma.exchange.create({ data: createExchangeDto });
  }

  findAll() {
    return this.prisma.exchange.findMany();
  }

  findOne(id: number) {
    return this.prisma.exchange.findUnique({ where: { id } });
  }

  update(id: number, updateExchangeDto: UpdateExchangeDto) {
    return `This action updates a #${id} exchange`;
  }

  remove(id: number) {
    return `This action removes a #${id} exchange`;
  }
}
