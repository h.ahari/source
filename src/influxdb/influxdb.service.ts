import { InfluxDB } from '@influxdata/influxdb-client';
import { Injectable } from '@nestjs/common';

@Injectable()
export class InfluxdbService {
  influxDB: InfluxDB;

  constructor() {
    const token = process.env.INFLUXDB_TOKEN
    const url = process.env.INFLUXDB_URL
    this.influxDB = new InfluxDB({ url: url, token: token });
  }
}
