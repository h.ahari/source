import { ApiProperty } from "@nestjs/swagger";

export class CreateMarketDto {
  @ApiProperty()
  name: string;

  @ApiProperty()
  baseId: number;

  @ApiProperty()
  exchangeId: number;

  @ApiProperty()
  quoteId: number;
}
