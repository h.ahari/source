import { ApiProperty } from "@nestjs/swagger";
import { Currency, Market } from "@prisma/client";

export class MarketEntity implements Market {
  @ApiProperty()
  id: number;

  @ApiProperty()
  name: string;

  @ApiProperty()
  baseId: number;

  @ApiProperty()
  exchangeId: number;

  @ApiProperty()
  quoteId: number;
}
