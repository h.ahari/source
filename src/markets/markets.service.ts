import { Injectable } from '@nestjs/common';
import { CreateMarketDto } from './dto/create-market.dto';
import { UpdateMarketDto } from './dto/update-market.dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class MarketsService {
  constructor(private prisma: PrismaService) { }

  create(createMarketDto: CreateMarketDto) {
    return this.prisma.market.create({ data: createMarketDto });
  }

  findAll() {
    return this.prisma.market.findMany({ include: { base: true, exchange: true, quote: true } });
  }

  findOne(id: number) {
    return this.prisma.market.findUnique({
      where: { id },
      include: { base: true, exchange: true, quote: true }
    });
  }

  update(id: number, updateMarketDto: UpdateMarketDto) {
    return `This action updates a #${id} market`;
  }

  remove(id: number) {
    return `This action removes a #${id} market`;
  }
}
