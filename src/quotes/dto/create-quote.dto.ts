import { ApiProperty } from "@nestjs/swagger"

export class CreateQuoteDto {
  @ApiProperty()
  name: string

  @ApiProperty()
  symbol: string
}
