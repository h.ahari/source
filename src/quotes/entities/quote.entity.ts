import { ApiProperty } from "@nestjs/swagger";
import { Quote } from "@prisma/client";

export class QuoteEntity implements Quote {
  @ApiProperty()
  id: number

  @ApiProperty()
  name: string

  @ApiProperty()
  symbol: string
}
