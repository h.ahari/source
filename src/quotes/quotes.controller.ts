import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { QuotesService } from './quotes.service';
import { CreateQuoteDto } from './dto/create-quote.dto';
import { UpdateQuoteDto } from './dto/update-quote.dto';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { QuoteEntity } from './entities/quote.entity';

@Controller('quotes')
@ApiTags('Quote')
export class QuotesController {
  constructor(private readonly quotesService: QuotesService) { }

  @Post()
  @ApiCreatedResponse({ type: QuoteEntity })
  create(@Body() createQuoteDto: CreateQuoteDto) {
    return this.quotesService.create(createQuoteDto);
  }

  @Get()
  @ApiOkResponse({ type: QuoteEntity, isArray: true })
  findAll() {
    return this.quotesService.findAll();
  }

  @Get(':id')
  @ApiOkResponse({ type: QuoteEntity })
  findOne(@Param('id') id: string) {
    return this.quotesService.findOne(+id);
  }

  @Patch(':id')
  @ApiOkResponse({ type: QuoteEntity })
  update(@Param('id') id: string, @Body() updateQuoteDto: UpdateQuoteDto) {
    return this.quotesService.update(+id, updateQuoteDto);
  }

  @Delete(':id')
  @ApiOkResponse({ type: QuoteEntity })
  remove(@Param('id') id: string) {
    return this.quotesService.remove(+id);
  }
}
