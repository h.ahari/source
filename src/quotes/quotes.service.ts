import { Injectable } from '@nestjs/common';
import { CreateQuoteDto } from './dto/create-quote.dto';
import { UpdateQuoteDto } from './dto/update-quote.dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class QuotesService {
  constructor(private prisma: PrismaService) { }

  create(createQuoteDto: CreateQuoteDto) {
    return this.prisma.quote.create({ data: createQuoteDto });
  }

  findAll() {
    return this.prisma.quote.findMany();
  }

  findOne(id: number) {
    return this.prisma.quote.findUnique({ where: { id } });
  }

  update(id: number, updateQuoteDto: UpdateQuoteDto) {
    return `This action updates a #${id} quote`;
  }

  remove(id: number) {
    return `This action removes a #${id} quote`;
  }
}
