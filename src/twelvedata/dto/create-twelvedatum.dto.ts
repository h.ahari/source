import { Optional } from "@nestjs/common";
import { ApiProperty } from "@nestjs/swagger";

export class CreateTwelvedatumDto {
  @ApiProperty({ default: "BTC" })
  base: string;

  @ApiProperty({ default: "USD" })
  quote: string;

  @ApiProperty({ default: "Binance" })
  exchange: string;

  @ApiProperty({ default: "1min" })
  interval: string;

  @ApiProperty({ default: "1980-12-12" })
  start_date: Date;

  @ApiProperty({ required: false })
  end_date: Date;
}
