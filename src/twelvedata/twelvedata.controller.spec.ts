import { Test, TestingModule } from '@nestjs/testing';
import { TwelvedataController } from './twelvedata.controller';
import { TwelvedataService } from './twelvedata.service';

describe('TwelvedataController', () => {
  let controller: TwelvedataController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TwelvedataController],
      providers: [TwelvedataService],
    }).compile();

    controller = module.get<TwelvedataController>(TwelvedataController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
