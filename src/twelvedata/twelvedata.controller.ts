import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { TwelvedataService } from './twelvedata.service';
import { CreateTwelvedatumDto } from './dto/create-twelvedatum.dto';

@Controller('twelvedata')
export class TwelvedataController {
  constructor(private readonly twelvedataService: TwelvedataService) {}

  @Get('data')
  getData(@Query() query: CreateTwelvedatumDto) {
    return this.twelvedataService.getData(query);
  }

  @Post('insert')
  insertData(@Body() createTwelvedatumDto: CreateTwelvedatumDto) {
    return this.twelvedataService.insertData(createTwelvedatumDto);
  }
}
