import { Test, TestingModule } from '@nestjs/testing';
import { TwelvedataGateway } from './twelvedata.gateway';

describe('TwelvedataGateway', () => {
  let gateway: TwelvedataGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TwelvedataGateway],
    }).compile();

    gateway = module.get<TwelvedataGateway>(TwelvedataGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
