import { Module } from '@nestjs/common';
import { TwelvedataService } from './twelvedata.service';
import { TwelvedataController } from './twelvedata.controller';
import { HttpModule } from '@nestjs/axios';
import { InfluxdbModule } from 'src/influxdb/influxdb.module';
import { TwelvedataGateway } from './twelvedata.gateway';

@Module({
  controllers: [TwelvedataController],
  providers: [TwelvedataGateway, TwelvedataService],
  imports: [
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 10000,
        maxRedirects: 20,
        baseURL: 'https://api.twelvedata.com',
        params: {
          apikey: process.env.TWELVEDATA_API_KEY,
        },
      })
    }),
    InfluxdbModule,
  ]
})
export class TwelvedataModule { }
