import { Test, TestingModule } from '@nestjs/testing';
import { TwelvedataService } from './twelvedata.service';

describe('TwelvedataService', () => {
  let service: TwelvedataService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TwelvedataService],
    }).compile();

    service = module.get<TwelvedataService>(TwelvedataService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
