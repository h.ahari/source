import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { InfluxdbService } from 'src/influxdb/influxdb.service';
import { Point, QueryApi, WriteApi } from '@influxdata/influxdb-client';
import { CreateTwelvedatumDto } from './dto/create-twelvedatum.dto';
import { catchError, firstValueFrom } from 'rxjs';
import { AxiosError } from 'axios';
import { TwelvedataGateway } from './twelvedata.gateway';
const WebSocket = require('ws');

@Injectable()
export class TwelvedataService {
  private readonly logger = new Logger(InfluxdbService.name);
  private writeClient: WriteApi;
  private queryClient: QueryApi;
  private bucket = 'ahari'

  constructor(
    private httpService: HttpService,
    private influxdbService: InfluxdbService,
    private twelvedataGateway: TwelvedataGateway
  ) {
    let org = `ahari`
    this.writeClient = this.influxdbService.influxDB.getWriteApi(org, this.bucket, 's')
    this.queryClient = this.influxdbService.influxDB.getQueryApi(org)

    const ws = new WebSocket(`wss://ws.twelvedata.com/v1/quotes/price?apikey=${process.env.TWELVEDATA_API_KEY}`);

    const newData = (props: { base: string, quote: string, exchange: string, price: number, datetime: number }) => {
      console.log(props);

      let fluxQuery = `
        from(bucket: "${this.bucket}")
          |> range(start: -1m)
          |> filter(fn: (r) => r["_measurement"] == "candlestick")
          |> filter(fn: (r) => r["base"] == "${props.base}")
          |> filter(fn: (r) => r["exchange"] == "${props.exchange}")`

      let result = []
      this.queryClient.queryRows(fluxQuery, {
        next: (row, tableMeta) => {
          const tableObject = tableMeta.toObject(row)
          result = [...result, tableObject]
        },
        error: (error) => {
          console.error('\nError', error)
        },
        complete: () => {
          let time = new Date((props.datetime - 12600) * 1000)
          time.setSeconds(0, 0)
          console.log(time);

          let point: Point
          if (result.length) {
            point = new Point('candlestick')
              .tag('base', props.base)
              .tag('quote', props.quote)
              .tag('interval', '1min')
              .tag('exchange', props.exchange)
              .tag('source', 'twelvedata')
              .floatField('open', parseFloat(result[0].open))
              .floatField('high', Math.max(result[0].high, props.price))
              .floatField('low', Math.min(result[0].low, props.price))
              .floatField('close', props.price)
              .timestamp(time.getTime() / 1000)
          } else {
            point = new Point('candlestick')
              .tag('base', props.base)
              .tag('quote', props.quote)
              .tag('interval', '1min')
              .tag('exchange', props.exchange)
              .tag('source', 'twelvedata')
              .floatField('open', props.price)
              .floatField('high', props.price)
              .floatField('low', props.price)
              .floatField('close', props.price)
              .timestamp(time.getTime() / 1000)
          }
          this.writeClient.writePoint(point)
          this.writeClient.flush()
          console.log(point.fields.open);
          
          twelvedataGateway.server.emit('events', {
            time: time.toISOString().split('T')[0],
            open: parseFloat(point.fields.open)
          })
        },
      })
    }

    ws.on('open', function open() {
      const message = {
        action: "subscribe",
        params: {
          symbols: [
            {
              symbol: "BTC/USD",
              exchange: "Binance"
            },
            {
              symbol: "ETH/USD",
              exchange: "Binance"
            },
          ]
        }
      };

      ws.send(JSON.stringify(message));
    });

    ws.on('message', function incoming(data) {
      let message = JSON.parse(data.toString())

      if (message.event === 'price') {
        newData({
          base: message.symbol.split('/')[0],
          quote: message.symbol.split('/')[1],
          exchange: message.exchange,
          price: message.price,
          datetime: message.timestamp,
        })
      }
    });

    ws.on('close', function close() {
      console.log('disconnected from /quotes/price');
    });

    ws.on('error', function error(err) {
      console.error('error:', err);
    });
  }

  async getData(query: CreateTwelvedatumDto): Promise<any[]> {
    return new Promise((resolve, reject) => {
      let result = []
      let fluxQuery = `
        data = () => from(bucket: "${this.bucket}")
          |> range(start: ${query.start_date}, stop: ${query.end_date ? query.end_date : "0m"})
          |> filter(fn: (r) => r["_measurement"] == "candlestick")
          |> filter(fn: (r) => r["base"] == "${query.base}")
          |> filter(fn: (r) => r["exchange"] == "${query.exchange}")

        open = data()
          |> filter(fn: (r) => r["_field"] == "open")
          |> aggregateWindow(every: ${query.interval}, fn: first, timeSrc: "_start")
          |> yield(name: "open")

        close = data()
          |> filter(fn: (r) => r["_field"] == "close")
          |> aggregateWindow(every: ${query.interval}, fn: last, timeSrc: "_start")
          |> yield(name: "close")

        high = data()
          |> filter(fn: (r) => r["_field"] == "high")
          |> aggregateWindow(every: ${query.interval}, fn: max, timeSrc: "_start")
          |> yield(name: "high")

        low = data()
          |> filter(fn: (r) => r["_field"] == "low")
          |> aggregateWindow(every: ${query.interval}, fn: min, timeSrc: "_start")
          |> yield(name: "low")
        
        union(tables: [open, high, low, close])
          |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
          |> filter(fn: (r) => exists r.open or exists r.close or exists r.high or exists r.low)
          |> sort(columns: ["_time"], desc: false)
          |> yield()
      `

      this.queryClient.queryRows(fluxQuery, {
        next: (row, tableMeta) => {
          const tableObject = tableMeta.toObject(row)
          if (tableObject.result === "_result") {
            result = [...result, {
              time: tableObject._time.split("T")[0],
              open: tableObject.open,
              close: tableObject.close,
              high: tableObject.high,
              low: tableObject.low,
            }]
          }
        },
        error: (error) => {
          console.error('\nError', error)
          reject(error)
        },
        complete: () => {
          resolve(result)
        },
      })

    })
  }

  async insertData(createTwelvedatumDto: CreateTwelvedatumDto) {
    let start_date = new Date(createTwelvedatumDto.start_date);
    start_date.setUTCHours(0, 0, 0, 0);

    // end date is same as start date + 3 day without change start_date
    let step_date = new Date(start_date.getTime() + 3 * 24 * 60 * 60 * 1000)

    // calculate the number of days between start_date and now
    const end_date = new Date(createTwelvedatumDto.end_date)
    end_date.setUTCHours(0, 0, 0, 0)

    // end date plus one day
    end_date.setDate(end_date.getDate() + 1)

    // for each async await every 3 day, from start date to now with 3 day steps and insert into influxdb
    while (start_date.getTime() < end_date.getTime()) {
      console.log(start_date);

      const { data: { values } } = await firstValueFrom(
        this.httpService.get('time_series', {
          params: {
            symbol: createTwelvedatumDto.base + "/" + createTwelvedatumDto.quote,
            exchange: createTwelvedatumDto.exchange,
            interval: createTwelvedatumDto.interval,
            start_date: start_date,
            end_date: step_date
          }
        }).pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw 'An error happened!';
          })
        ))

      let points = await values.map((item: any) => {
        let point = new Point('candlestick')
          .tag('base', createTwelvedatumDto.base)
          .tag('quote', createTwelvedatumDto.quote)
          .tag('interval', createTwelvedatumDto.interval)
          .tag('exchange', createTwelvedatumDto.exchange)
          .tag('source', 'twelvedata')
          .floatField('open', parseFloat(item.open))
          .floatField('high', parseFloat(item.high))
          .floatField('low', parseFloat(item.low))
          .floatField('close', parseFloat(item.close))
          .timestamp(new Date(item.datetime).getTime() / 1000)
        return point
      })

      this.writeClient.writePoints(points)
      await this.writeClient.flush()
      start_date = step_date
      step_date = new Date(step_date.getTime() + 3 * 24 * 60 * 60 * 1000)

      await this.delay(5500)
    }
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
